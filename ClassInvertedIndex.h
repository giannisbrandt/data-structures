#ifndef CLASSINVERTEDINDEX_H
#define CLASSINVERTEDINDEX_H
#include <fstream>
#include <iostream>
#include "AVL.h"

using namespace std;




class ClassInvertedIndex : public AVL
{
    public:
        ClassInvertedIndex();
        virtual ~ClassInvertedIndex();
        void write_index(InitialNode *rootin,std::ofstream &output);
        void fileread(std::ifstream &file,std::ifstream &com,Node *root,ClassInvertedIndex obj);

    protected:
    private:


};

#endif // CLASSINVERTEDINDEX_H