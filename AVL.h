#ifndef AVL_H
#define AVL_H
#include "Node.h"
#include "InitialNode.h"

class AVL : public InitialNode
{
    public:
        AVL();
        virtual ~AVL();
        int subtreesheight(Node *root);
        int heightdif(Node *root);
        Node* balanceroot(Node *root);
        Node* lrotation(Node *root);
        Node* rrotation(Node *root);
        Node* lrrotation(Node *root);
        Node* rlrotation(Node *root);
        Node* insert(Node *root,int key);
        Node* searchmin(Node* root);
        int preorder(Node* root,int key);

    protected:
    private:
};

#endif // AVL_H