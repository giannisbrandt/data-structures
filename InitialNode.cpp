#include "InitialNode.h"
#include <iostream>
#include <fstream>

using namespace std;

InitialNode::InitialNode()
{
   //ctor
}

InitialNode::~InitialNode()
{
    //dtor
}

int InitialNode::subtreesheight(InitialNode *root)
{
    int lheight=0,rheight=0,maxx=0,maxh=0;
    if (root!=NULL)
    {
        lheight=subtreesheight(root->left);
        rheight=subtreesheight(root->right);
        maxx=lheight;
        if (rheight>lheight)
            maxx=rheight;
        maxh=maxx+1;
    }
    return maxh;
}

int InitialNode::heightdif(InitialNode *root)
{

   int llheight=0,rrheight=0,dif=0;
   llheight=subtreesheight(root->left);
   rrheight=subtreesheight(root->right);
   dif=llheight-rrheight;
   return dif;
}

InitialNode *InitialNode::balanceroot(InitialNode *root)
{
    int diff=0;
    diff=heightdif(root);
    if (diff>1){
        if (heightdif(root->left)>0)
           root=lrotation(root);
        else
            root=lrrotation(root);
    }

    else if (diff<-1){
        if (heightdif(root->right)<0)
            root=rrotation(root);
        else
            root=rlrotation(root);
    }


    return root;
}

InitialNode *InitialNode::lrotation(InitialNode* root)
{
    InitialNode *temp;
    temp=root->left;
    root->left=temp->right;
    temp->right=root;

    return temp;
}

InitialNode *InitialNode::rrotation(InitialNode *root)
{
        InitialNode *temp;
        temp=root->right;
        root->right=temp->left;
        temp->left=root;
        //root->right=NULL;
        //root=temp;
        return temp;
}

InitialNode *InitialNode::rlrotation(InitialNode *root)
{
    InitialNode *temp;
    temp=root->right;
    root->right=lrotation(temp);
    return rrotation(root);
}

InitialNode *InitialNode::lrrotation(InitialNode *root)
{
    InitialNode *temp;
    temp=root->left;
    root->left=rrotation(temp);
    return lrotation(root);
}


InitialNode* InitialNode::insertneighboor(InitialNode *root,int key,int identity)
{
    if (root==NULL)
    {
        root = new InitialNode;
        root->ID=identity;
        root->data=key;
        root->left=NULL;
        root->right=NULL;
    }
    else if (identity>root->ID)
    {
        root->right=insertneighboor(root->right,key,identity);
        root=balanceroot(root);
    }
    else if (identity<=root->ID)
    {
        root->left=insertneighboor(root->left,key,identity);
        root=balanceroot(root);
    }
    return root;
}

void InitialNode::preorder1(InitialNode *rootin,std::ofstream &output,int i)
{
       if (rootin==NULL){
            return;
       }
       if (i==rootin->ID){
          //output<<rootin->ID<<"-";
          output<<rootin->data<<" , ";
          preorder1(rootin->left,output,i);
          preorder1(rootin->right,output,i);

       }
       else if (i!=rootin->ID)
       {
           //output<<endl;
           preorder1(rootin->left,output,i);
           preorder1(rootin->right,output,i);

       }
      //output<<endl;
}

int InitialNode::preorderid(InitialNode* rootin,int id)
{
    if (rootin==NULL)
        return 0;
    if (rootin->ID==id)
        return 1;
    else
    {
        if (id>=rootin->ID)
            preorderid(rootin->right,id);
        else
            preorderid(rootin->left,id);
    }
}

int InitialNode::preorderneighboor(InitialNode *rootin,int buffer1,int buffer2)
{

    if (rootin==NULL)
        return 0;
    if (rootin->ID==buffer1 && rootin->data==buffer2)
        return 1;
    else
    {
        if (buffer1>=rootin->ID )
            preorderneighboor(rootin->right,buffer1,buffer2);
        else
            preorderneighboor(rootin->left,buffer1,buffer2);
//        neighboor++;
    }
}


void InitialNode::preorderdata(InitialNode* rootin,int i,int &p){

    if (rootin==NULL){
            return;
       }
       if (i==rootin->ID){
            p++;
          preorderdata(rootin->left,i,p);
          preorderdata(rootin->right,i,p);
       }
       else if (i!=rootin->ID)
       {
           preorderdata(rootin->left,i,p);
           preorderdata(rootin->right,i,p);
       }
}
int InitialNode::searchmaxID(InitialNode *rootin,int maxid,int left,int right)
{
    if (rootin==NULL)
        return maxid;

    if (rootin->ID>=maxid)
    {
        maxid=rootin->ID;
        left=searchmaxID(rootin->left,maxid,left,right);
        right=searchmaxID(rootin->right,maxid,left,right);
        if (left>right)
            maxid=left;
        else
            maxid=right;
    }
    else
    {
        left=searchmaxID(rootin->left,maxid,left,right);
        right=searchmaxID(rootin->right,maxid,left,right);
    }
    return maxid;
}

int InitialNode::searchminID(InitialNode *rootin,int minid,int left,int right)
{
    if (rootin==NULL)
        return minid;

    if (rootin->ID>=minid)
    {
        minid=rootin->ID;
        left=searchmaxID(rootin->left,minid,left,right);
        right=searchmaxID(rootin->right,minid,left,right);
        if (left<right)
            minid=left;
        else
            minid=right;
    }
    else
    {
        left=searchmaxID(rootin->left,minid,left,right);
        right=searchmaxID(rootin->right,minid,left,right);
    }
    return minid;
}

