#include "AVL.h"
#include <iostream>
#include "Node.h"

using namespace std;

AVL::AVL()
{

}

AVL::~AVL()
{
    //dtor
}

int AVL::subtreesheight(Node *root)
{
    int lheight=0,rheight=0,maxx=0,maxh=0;
    if (root!=NULL)
    {
        lheight=subtreesheight(root->left);
        rheight=subtreesheight(root->right);
        maxx=lheight;
        if (rheight>lheight)
            maxx=rheight;
        maxh=maxx+1;
    }
    return maxh;
}

int AVL::heightdif(Node *root)
{

   int llheight=0,rrheight=0,dif=0;
   llheight=subtreesheight(root->left);
   rrheight=subtreesheight(root->right);
   dif=llheight-rrheight;
   return dif;
}


Node *AVL::balanceroot(Node *root)
{
    int diff=0;
    diff=heightdif(root);
    if (diff>1){
        if (heightdif(root->left)>0)
           root=lrotation(root);
        else
            root=lrrotation(root);
    }

    else if (diff<-1){
        if (heightdif(root->right)<0)
            root=rrotation(root);
        else
            root=rlrotation(root);
    }


    return root;
}

Node *AVL::lrotation(Node* root)
{
    Node *temp;
    temp=root->left;
    root->left=temp->right;
    temp->right=root;

    return temp;
}

Node *AVL::rrotation(Node *root)
{
        Node *temp;
        temp=root->right;
        root->right=temp->left;
        temp->left=root;
        return temp;
}

Node *AVL::rlrotation(Node *root)
{
    Node *temp;
    temp=root->right;
    root->right=lrotation(temp);
    return rrotation(root);
}

Node *AVL::lrrotation(Node *root)
{
    Node *temp;
    temp=root->left;
    root->left=rrotation(temp);
    return lrotation(root);
}

Node *AVL::insert(Node *root,int key){
    if (root==NULL)
    {
        root = new Node;
        root->data=key;
        root->left=NULL;
        root->right=NULL;
    }
    else if (key>root->data)
    {
        root->right=insert(root->right,key);
        root=balanceroot(root);
    }
    else if (key<=root->data)
    {
        root->left=insert(root->left,key);
        root=balanceroot(root);
    }
    return root;
}

int AVL::preorder(Node* root,int key)
{
    if (root==NULL)
        return 0;
    if (root->data==key)
        return 1;
    else
    {
        preorder(root->left,key);
        preorder(root->right,key);
    }
}