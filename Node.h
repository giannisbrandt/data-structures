#ifndef NODE_H
#define NODE_H
#include <iostream>


class Node
{
    public:
        Node();
        virtual ~Node();
        Node *left;
        Node *right;
        int data;
    protected:
    private:
};

#endif // NODE_H