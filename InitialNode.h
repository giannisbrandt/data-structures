#ifndef INITIALNODE_H
#define INITIALNODE_H
#include "Node.h"

#include <fstream>



class InitialNode
{
    public:
        InitialNode();
        virtual ~InitialNode();
        InitialNode *left;
        InitialNode *right;
        int data;
        int ID;
        int subtreesheight(InitialNode *root);
        int heightdif(InitialNode *root);
        InitialNode* balanceroot(InitialNode *root);
        InitialNode* lrotation(InitialNode *root);
        InitialNode* rrotation(InitialNode *root);
        InitialNode* lrrotation(InitialNode *root);
        InitialNode* rlrotation(InitialNode *root);
        InitialNode* insertneighboor(InitialNode *root,int key,int intentity);
        void preorder1(InitialNode *rootin,std::ofstream &output,int i);
        int preorderid(InitialNode* rootin,int id);
        int preorderneighboor(InitialNode* rootin,int buffer1,int buffer2);
        void preorderdata(InitialNode* rootin,int i,int &p);
        int searchmaxID(InitialNode* rootin,int maxid,int left,int right);
        int searchminID(InitialNode* rootin,int minid,int left,int right);
        InitialNode* searchmin(InitialNode* rootin);
        InitialNode *rootin;
    protected:
    private:

};

#endif // INITIALNODE_H